package lab9.event;

// Import required modules
import java.util.Calendar;
import java.math.BigInteger;
import java.text.SimpleDateFormat;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;
    protected Calendar startTime;
    protected Calendar stopTime;
    private BigInteger cost_hour;
    
    public Event(String name, Calendar startTime, Calendar stopTime, BigInteger cost_hour){
        this.name = name;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.cost_hour = cost_hour;
    }

    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    
    public String getName()
    {
        return this.name;
    }

    /**
    * Accessor for cost field. 
    * @return amount cost / hour
    */

    public BigInteger getCost(){
        return this.cost_hour;
    }


    public String toString(){
        SimpleDateFormat time = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");
        String return_str = this.name
                + "\n" + "Waktu mulai: " + time.format(startTime.getTime())
                + "\n" + "Waktu selesai: " + time.format(stopTime.getTime())
                + "\n" + "Biaya kehadiran: " + cost_hour;
        return return_str;     
    }


    public boolean checkEvent(Event event) {
        if(!this.startTime.after(event.stopTime) && !this.stopTime.before(event.startTime)){
            return true;
        }
        return false;
    }

    public int compareTo(Event event) {
        if (this.startTime.equals(event.startTime)) return 0;
        else if (this.startTime.after(event.startTime)) return 1;
        return -1;
    }
}
