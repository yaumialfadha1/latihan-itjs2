package character;

import java.util.ArrayList;

public class Player {
    protected String name;
    protected int hp;
    protected boolean mati;
    protected boolean mateng;
    protected ArrayList<Player> diet = new ArrayList<>();

    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
        mateng = false;
        mati = false;
        if (hp <= 0) {
            mati = true;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public boolean getMatang() {
        return mateng;
    }

    public void setMatang(boolean matang) {
        mateng = matang;
    }

    public boolean getDead() {
        return mati;
    }

    public void setDead(boolean dead) {
        mati = dead;
    }

    public ArrayList<Player> getDiet() {
        return diet;
    }

    public void setDiet(ArrayList<Player> diet) {
        this.diet = diet;
    }

    public String getTipe() {
        return "Player";
    }

    public void attacked() {
        this.hp -= 10;
        if (hp <= 0) {
            hp = 0;
            mati = true;
        }
    }

    public void attack(Player target) {
        if (mati) {
            return;
        }
        target.attacked();
    }

    public void burn(Player target) {
        if (mati) {
            return;
        }
        target.burned();
    }

    public void burned() {
        this.hp -= 10;
        if (hp <= 0) {
            hp = 0;
            mati = true;
            mateng = true;
        }
    }

    public void eat(Player target) {
        this.hp += 15;
        this.diet.add(target);
    }

    public boolean canEat(Player target) {
        return true;
    }

}
