package character;

public class Monster extends Player {

    String roar;

    public Monster(String name, int hp) {
        super(name, hp * 2);
        this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
    }

    public Monster(String name, int hp, String roar) {
        super(name, hp * 2);
        this.roar = roar;
    }

    public String roar() {
        return roar;
    }

    @Override
    public boolean canEat(Player target) {
        if (target.getDead()) {
            return true;
        }
        return false;
    }

    @Override
    public String getTipe() {
        return "Monster";
    }
}
//  write Monster Class here
