import java.util.Scanner;


public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		
			System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
					"--------------------\n" +
					"Nama Kepala Keluarga   : ");
			String nama = input.nextLine();
			System.out.print("Alamat Rumah           : ");
			String alamat = input.nextLine();
			System.out.print("Panjang Tubuh (cm)     : ");
			int panjang = input.nextInt();
			System.out.print("Lebar Tubuh (cm)       : ");
			int lebar = input.nextInt();
			System.out.print("Tinggi Tubuh (cm)      : ");
			int tinggi = input.nextInt();
			System.out.print("Berat Tubuh (kg)       : ");
			double berat = input.nextDouble();
			System.out.print("Jumlah Anggota Keluarga: ");
			int keluarga = input.nextInt();
			input.nextLine();
			System.out.print("Tanggal Lahir          : ");
			String tanggalLahir = input.nextLine();
			System.out.print("Catatan Tambahan       : ");
			String catatan = input.nextLine();
			System.out.print("Jumlah Cetakan Data    : ");
			int jumlahCetakan = input.nextInt();
		


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		double rasio = berat/(((double)panjang/100)*((double)lebar/100)*((double)tinggi/100));

		input.nextLine();
		for (int i =0; i<jumlahCetakan; i++ ) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Penerima Data		:");
			String penerima = input.nextLine();
			String penerima1 = penerima.toUpperCase();
			System.out.println("");
			System.out.println("Pencetakan " + (i+1) + " dari " + jumlahCetakan + " untuk: "+ penerima);
			System.out.println("DATA SIAP DICETAK UNTUK ARSIP "+ penerima1);
			//String penerima = .....; // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			if (catatan.length() > 0) catatan = catatan;
			else catatan = "Tidak ada catatan tambahan";

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			System.out.println("--------------------");
			System.out.println(nama + " - "+ alamat);
			System.out.println("lahir pada tanggal "+ tanggalLahir);
			System.out.println("Rasio Berat per Volume	= "+ (short) rasio+" kg/m^3");
			System.out.println("Catatan: " + catatan);
		}

		
		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)



		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		int Ascii = 0;
		for (int i = 0; i< nama.length(); i++){
			int nama1 = (int) (nama.charAt(i));
			Ascii += nama1;
		}
		String  nama1 = String.valueOf(nama.charAt(0));
		int Volume = (panjang*lebar*tinggi);
		int Kalkulasi = (Volume+Ascii)%10000;
		
		String nomerKeluarga = nama1+Kalkulasi;
		
		
		if (panjang <0 || panjang >= 250){
			System.out.println("\n"+"WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		if (lebar <0 || lebar >= 250){
			System.out.println("\n"+"WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		if (tinggi <0 || tinggi >= 250){
			System.out.println("\n"+"WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		if (berat < 0 || berat >= 150){
			System.out.println("\n"+"WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		if (keluarga <0 || keluarga >= 20){
			System.out.println("\n"+"WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}
		

		
		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = 50000*365*keluarga;

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		String[] tahunLahir = tanggalLahir.split("-"); // lihat hint jika bingung
		int tahunLahir2 = Integer.valueOf(tahunLahir[2]);
		int umur = 2018 - tahunLahir2;
		
		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		
		String namaAP = "" ;
		String Kabupaten = "";
		
		// untuk mengelompokkan apartemen
		if (umur>=0 && umur <=18){
			namaAP = "PPMT";
			Kabupaten = "Rotunda";
		}else if ((umur>=19 && umur <=1018)&&(anggaran >= 0 && anggaran <= 100000000)){
			namaAP = "Teksas";
			Kabupaten = "Sastra";
		}else if ((umur>=19 && umur <=1018)&&(anggaran >= 100000000)){
			namaAP = "Mares";
			Kabupaten = "Margonda";
		}
		System.out.println("\n"+"REKOMENDASI APARTEMEN"+"\n"+"--------------------"+"\n"+
		"MENGETAHUI: Identitas keluarga: "+nama+" - "+nomerKeluarga+ "\n"+
		"MENIMBANG:  Anggaran makanan tahunan: Rp"+ anggaran+ "\n"+
										"Umur kepala keluarga: "+umur+" tahun"+"\n"+
							"MEMUTUSKAN: keluarga "+nama+" akan ditempatkan di:"+"\n"+namaAP+ " kabupaten "+ Kabupaten);
			
		
		input.close();
	}
}