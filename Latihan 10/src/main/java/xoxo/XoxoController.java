package xoxo;

import java.awt.event.*;
import xoxo.crypto.*;
import xoxo.exceptions.*;
import xoxo.key.*;
import xoxo.util.*;

import static java.nio.file.StandardOpenOption.*;
import java.nio.file.*;
import java.io.*;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Yaumi Alfadha
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    private static int enCounter = 0, deCounter = 0, logCounter = 0;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setEncryptFunction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String messageInput = gui.getMessageText();
                String keyInput = gui.getKeyText();
                String seedInput = gui.getSeedText();
                int seed = HugKey.DEFAULT_SEED;

                // check seed
                if (!seedInput.equals("")) {
                    try {
                        seed = Integer.parseInt(seedInput);
                    }
                    catch (NumberFormatException nfe) {
                        gui.warningWindow("Seed input error, " + nfe.getMessage());
                        return;
                    }
                }

                // check message, key, size, etc
                try {
                    XoxoEncryption en = new XoxoEncryption(keyInput);
                    XoxoMessage enMessage = en.encrypt(messageInput, seed);
                    byte d[] = enMessage.getEncryptedMessage().getBytes();
                    Path path = Paths.get("./encrypted message - " + ++enCounter + ".enc");

                    try {
                        OutputStream outputFile = new BufferedOutputStream(
                            Files.newOutputStream(path, CREATE, APPEND));
                        outputFile.write(d, 0, d.length);
                        outputFile.flush();
                        outputFile.close();
                    }
                    catch (IOException ioe) {
                        gui.warningWindow("Output error, " + ioe.getMessage());
                        return;
                    }

                    gui.appendLog(++logCounter + " - encrypted message in " + path.toString());
                }
                catch (InvalidCharacterException ice) {
                    gui.warningWindow(ice.getMessage());
                    gui.appendLog("Invalid Character, " + ice.getMessage());
                } 
                catch (KeyTooLongException ktle) {
                    gui.warningWindow(ktle.getMessage());
                    gui.appendLog("Key Too Long: " + ktle.getMessage());
                } 
                catch (RangeExceededException ree) {
                    gui.warningWindow(ree.getMessage());
                    gui.appendLog("Range Exceeded: " + ree.getMessage());
                } 
                catch (SizeTooBigException stbe) {
                    gui.warningWindow(stbe.getMessage());
                    gui.appendLog("Size Too Big Exception: " + stbe.getMessage());
                } 
            }
        });

        gui.setDecryptFunction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String messageInput = gui.getMessageText();
                String keyInput = gui.getKeyText();
                String seedInput = gui.getSeedText();
                int seed = HugKey.DEFAULT_SEED;
                if (!seedInput.equals("")) {
                    try {
                        seed = Integer.parseInt(seedInput);
                    } 
                    catch (NumberFormatException nfe) {
                        gui.warningWindow(nfe.getMessage());
                        return;
                    }
                }
                XoxoDecryption de = new XoxoDecryption(keyInput);
                String deMessage = de.decrypt(messageInput, seed);
                byte d[] = deMessage.getBytes();
                Path path = Paths.get("./decrypted message - " + ++deCounter + ".txt");
                try {
                    OutputStream outputFile = new BufferedOutputStream(
                        Files.newOutputStream(path, CREATE, APPEND));
                    outputFile.write(d, 0, d.length);
                    outputFile.flush();
                    outputFile.close();
                } 
                catch (IOException ioe) {
                    gui.warningWindow("Output error, " + ioe.getMessage());
                    return;
                }
                gui.appendLog(++logCounter + " - decrypted message in " + path.toString());
            }
        });
    }
}