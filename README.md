# Dokumentasi dan Latihan
-Dokumentasi dan lab tiap bab, silahkan diliat2 hehehehhehehehe<br>
-Ditiap link terdapat dokumentasi step by step dan terdapat latihan<br>
-untuk solusi bisa cek di dalam foldernya,<br>
-kerjakan tiap latihan secara berurut<br>


## Daftar Isi


1. latihan
    1. [latihan 1](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_1/README.md) - Pengenalan Java & Git
    2. [latihan 2](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_2/README.md) - Konsep Dasar Pemrograman Java
    3. [latihan 3](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_3/README.md) - Rekursif
    4. [latihan 4](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_4/README.md) - Object Oriented Programming
    5. [latihan 5](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_5/README.md) - Array dan ArrayList
	6. [latihan 6](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_6/README.md) - Studi Kasus OOP
	7. [latihan 7](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_7/README.md) - Inheritance
	8. [latihan 8](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_8/README.md) - Polymorphism
	9. [latihan 9](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_9/README.md) - Packaging dan API
    10. [latihan 10](https://gitlab.com/DDP2-CSUI/ddp-lab/blob/master/lab_instructions/lab_10/README.md) - Exception dan GUI


