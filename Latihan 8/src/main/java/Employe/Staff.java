package Employe;

public class Staff extends Employe {

    private int maxBawahan;

    public Staff(String nama, int gaji) {
        super(nama, gaji);
        this.maxBawahan = 10;
    }

    public int getMaxBawahan() {return maxBawahan;}

    public boolean canRecruit(Employe employe) {
        if (employe instanceof Intern) {
            return true;
        }
        return false;
    }
}
