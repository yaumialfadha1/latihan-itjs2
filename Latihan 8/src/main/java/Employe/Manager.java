package Employe;

public class Manager extends Employe {

    private int maxBawahan;

    public Manager(String nama, int gaji) {
        super(nama, gaji);
        this.maxBawahan = 10;
    }

    public int getMaxBawahan() {return maxBawahan;}

    public boolean canRecruit(Employe employe) {
        if (employe instanceof Staff || employe instanceof Intern) {
			return true;}
        return false;
    }
}
