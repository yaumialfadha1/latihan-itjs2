package Employe;

public class Intern extends Employe {

    private int maxBawahan;

    public Intern(String nama, int gaji) {
        super(nama, gaji);
        this.maxBawahan = 0;
    }

    public int getMaxBawahan() {return maxBawahan;}

    public boolean canRecruit(Employe employe) {return false;}
}