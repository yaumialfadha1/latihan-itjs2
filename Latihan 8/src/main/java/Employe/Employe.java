package Employe;

import java.util.ArrayList;

public abstract class Employe {
    private String nama;
    private int gaji;
    private int gajiCounter = 0;
    private ArrayList<Employe> bawahan = new ArrayList<Employe>();

    public Employe(String nama, int gaji) {
        this.nama = nama;
        this.gaji = gaji;
    }
//getter
    public String getNama() {return nama;}

    public int getGaji() {return gaji;}

    public int getGajiCounter() {return gajiCounter;}

    public ArrayList<Employe> getBawahan() {return bawahan;}

    public void setBawahan(ArrayList<Employe> bawahan) {this.bawahan = bawahan;}

    public abstract int getMaxBawahan();

    public void addBawahan(Employe employe) {bawahan.add(employe);}

    public abstract boolean canRecruit(Employe employe);

    public void raiseSalary() {
        int newGaji = (gaji / 10) + gaji;
        System.out.println(nama + " mengalami kenaikan gaji sebesar 10% dari " + gaji + " menjadi " + newGaji);
        gaji = newGaji;
    }

    public void payDay() {
        gajiCounter++;
        if (gajiCounter == 6) {
            raiseSalary();
            gajiCounter = 0;
        }
    }
}
