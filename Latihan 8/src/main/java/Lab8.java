import java.util.Scanner;

import Corporate.*;
public class Lab8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while (input.hasNextLine()) {
            String[] input_split = input.nextLine().split(" ");
            if (input_split[0].toUpperCase().equals("TAMBAH_KARYAWAN")) {
                Corporate.add(input_split[2].toUpperCase(), input_split[1].toUpperCase(),
                        Integer.parseInt(input_split[3]));
            } else if (input_split[0].toUpperCase().equals("STATUS")) {
                Corporate.status(input_split[1]);
            } else if (input_split[0].toUpperCase().equals("GAJIAN")) {
                Corporate.payDay();
            } else if (input_split[0].toUpperCase().equals("TAMBAH_BAWAHAN")) {
                Corporate.addBawahan(input_split[1].toUpperCase(), input_split[2].toUpperCase());
            } else {
                System.out.println("Perintah yang dimasukan salah!");
            }

        }
    }

}
