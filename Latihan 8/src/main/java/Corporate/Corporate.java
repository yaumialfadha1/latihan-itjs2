package Corporate;

import java.util.ArrayList;

import Employe.*;

public class Corporate {
    private static final int Max_Employe = 10000;
    private static final int Max_Gaji_Staff = 18000;
    private static ArrayList<Employe> employer = new ArrayList<Employe>();

    public static Employe find(String nama) {
        for (Employe employ : employer) {
            if (nama.equalsIgnoreCase(employ.getNama())) {return employ;}
        }
        return null;
    }

    public static boolean check(Employe employ) {
        if (employ == null){return false;}
        else{return true;}
    }

    public static void add(String posisi, String nama, int gaji) {
        Employe employe = find(nama);
        if (employer.size() < Max_Employe) {
            if (check(employe)) {
                System.out.println("Karyawan dengan nama " + nama + " telah terdaftar");
            } else if (!check(employe)) {
                switch (posisi) {
                    case "MANAGER":
                        employer.add(new Manager(nama, gaji));
                        break;
                    case "STAFF":
                        employer.add(new Staff(nama, gaji));
                        break;
                    case "INTERN":
                        employer.add(new Intern(nama, gaji));
                        break;
                }
                System.out.println(nama + " mulai bekerja sebagai " + posisi + " di PT TAMPAN");
            } else {
                System.out.println("Jumlah karyawan PT TAMPAN sudah penuh..");
            }
        }
    }

    public static void status(String nama) {
        Employe employe = find(nama);
        if (check(employe)) {
            System.out.println(employe.getNama() + " " + employe.getGaji());
        } else {
            System.out.println("Karyawan tidak terdaftar");
        }
    }

    public static void addBawahan(String employ, String subordinateMember) {
        Employe karyawan = find(employ);
        Employe bawahan = find(subordinateMember);
        if (check(karyawan) && check(bawahan)) {
            if ((karyawan.getBawahan().size() < karyawan.getMaxBawahan()) && karyawan.canRecruit(bawahan)) {
                if (karyawan.getBawahan().contains(bawahan)) {
                    System.out.println("Karyawan " + subordinateMember + " telah menjadi bawahan " + employ);
                } else {
                    karyawan.addBawahan(bawahan);
                    System.out.println("Karyawan " + subordinateMember + " berhasil ditambahkan menjadi bawahan " + employ);
                }
            } else {
                if (karyawan.getBawahan().size() > karyawan.getMaxBawahan()) {
                    System.out.println("Jumlah bawahan Anda sudah melebihi batas maksimal");
                } else if (!karyawan.canRecruit(bawahan)) {
                    System.out.println("Anda tidak layak memiliki bawahan");
                }
            }
        } else {
            System.out.println("Nama tidak berhasil ditemukan");
        }
    }

    public static void payDay() {
        for (Employe employ : employer) {
            employ.payDay();
            if (employ instanceof Staff && employ.getGaji() > Max_Gaji_Staff) {
                employer.set(employer.indexOf(employ), new Manager(employ.getNama(), employ.getGaji()));
                System.out.println("Selamat, " + employ.getNama() + " telah dipromosikan menjadi MANAGER");
            }
        }
        System.out.println("Semua karyawan telah diberikan gaji");
    }
}
