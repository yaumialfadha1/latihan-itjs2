/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */
import java.util.*;
public class BingoCard {
    private Number[][] numbers;
    private Number[] numberStates;
    private int jumlahRestart = 0;
    private boolean isBingo;

    public BingoCard(Number[][] numbers, Number[] numberStates) {
        this.numbers = numbers;
        this.numberStates = numberStates;
        this.isBingo = false;
    }
    public Number[][] getNumbers() {
        return numbers;
    }
    public void setNumbers(Number[][] numbers) {
        this.numbers = numbers;
    }
    public Number[] getNumberStates() {
        return numberStates;
    }
    public void setNumberStates(Number[] numberStates) {
        this.numberStates = numberStates;
    }
    public boolean isBingo() {
        return isBingo;
    }
    public void setBingo(boolean isBingo) {
        this.isBingo = isBingo;
    }
    public void testWinHorizontally(){
        // Bingo Horizontal
        boolean h1 = this.numbers[0][0].isChecked() && this.numbers[0][1].isChecked() && this.numbers[0][2].isChecked()
                && this.numbers[0][3].isChecked()   && this.numbers[0][4].isChecked();
        boolean h2 = this.numbers[1][0].isChecked() && this.numbers[1][1].isChecked() && this.numbers[1][2].isChecked()
                && this.numbers[1][3].isChecked()   && this.numbers[1][4].isChecked();
        boolean h3 = this.numbers[2][0].isChecked() && this.numbers[2][1].isChecked() && this.numbers[2][2].isChecked()
                && this.numbers[2][3].isChecked()   && this.numbers[2][4].isChecked();
        boolean h4 = this.numbers[3][0].isChecked() && this.numbers[3][1].isChecked() && this.numbers[3][2].isChecked()
                && this.numbers[3][3].isChecked()   && this.numbers[3][4].isChecked();
        boolean h5 = this.numbers[4][0].isChecked() && this.numbers[4][1].isChecked() && this.numbers[4][2].isChecked()
                && this.numbers[4][3].isChecked()   && this.numbers[4][4].isChecked();
        if (h1 || h2 ||h3 ||h4 ||h5 ) {
            System.out.println("BINGO!");
            setBingo(true);
            System.out.println(info());
        }
    }
    public void testWinVertically(){
        // Bingo Vertical
        boolean v1 = this.numbers[0][0].isChecked() && this.numbers[1][0].isChecked() && this.numbers[2][0].isChecked() && this.numbers[3][0].isChecked()
                && this.numbers[4][0].isChecked();
        boolean v2 = this.numbers[0][1].isChecked() && this.numbers[1][1].isChecked() && this.numbers[2][1].isChecked() && this.numbers[3][1].isChecked()
                && this.numbers[4][1].isChecked();
        boolean v3 = this.numbers[0][2].isChecked() && this.numbers[1][2].isChecked() && this.numbers[2][2].isChecked() && this.numbers[3][2].isChecked()
                && this.numbers[4][2].isChecked();
        boolean v4 = this.numbers[0][3].isChecked() && this.numbers[1][3].isChecked() && this.numbers[2][3].isChecked() && this.numbers[3][3].isChecked()
                && this.numbers[4][3].isChecked();
        boolean v5 = this.numbers[0][4].isChecked() && this.numbers[1][4].isChecked() && this.numbers[2][4].isChecked() && this.numbers[3][4].isChecked()
                && this.numbers[4][4].isChecked();
        if (v1 || v2 || v3 || v4 || v5 ){
            System.out.println("BINGO!");
            setBingo(true);
            System.out.println(info());
        }
    }
    public void testWinDiagonal1(){
        // BIngo Diagonal
        boolean d1 = this.numbers[0][0].isChecked() && this.numbers[1][1].isChecked() && this.numbers[2][2].isChecked() && this.numbers[3][3].isChecked()
                && this.numbers[4][4].isChecked();
        if (d1){
            System.out.println("BINGO!");
            setBingo(true);
            System.out.println(info());
        }

    }
    public void testWinDiagonal2(){
        boolean d2 = this.numbers[4][0].isChecked() && this.numbers[3][1].isChecked() && this.numbers[2][2].isChecked() && this.numbers[1][3].isChecked()
                && this.numbers[0][4].isChecked();
        if (d2 ){
            System.out.println("BINGO!");
            setBingo(true);
            System.out.println(info());
        }
    }
    public String markNum(int num){
        //TODO Implement
        String hasil = "";
        for (int i = 0;i<5 ;i++ ) {
            for (int j = 0;j<5 ;j++ ) {
                if (this.numbers[i][j].getValue() == num && this.numbers[i][j].isChecked() ==true)
                    hasil = this.numbers[i][j].getValue() + " sebelumnya sudah tersilang";
                else if (this.numbers[i][j].getValue() == num && this.numbers[i][j].isChecked() == false) {
                    this.numbers[i][j].setChecked(true);
                    hasil = this.numbers[i][j].getValue() + " tersilang";
                }
            }
        }
        if (hasil == "")
            hasil = "Kartu tidak memiliki angka "+num;
        return hasil;
    }
    public String info(){
        //TODO Implement
        String hasil = "";
        for (int i = 0;i<5 ;i++ ) {
            for (int j = 0;j<5 ;j++ ) {
                if (this.numbers[i][j].isChecked() == true) hasil += "| X  ";
                else hasil += "| " + this.numbers[i][j].getValue() + " ";
                if (j == 4 ) {
                    if (i<4) hasil+="|\n";
                    else hasil+="|";
                }
            }
        }
        return hasil;
    }
    public void restart(){
        //TODO Implement
        if (jumlahRestart == 1) System.out.println("Sudah pernah mengajukan restart");
        else{
            for (int i = 0;i<5 ;i++ ) {
                for (int j = 0;j<5 ;j++ ) {
                    this.numbers[i][j].setChecked(false);
                }
            }
            System.out.println("Mulligan!");
            jumlahRestart = 1;
        }
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Number[][] numbers = new Number[5][5];
        Number[] nomorOri = new Number[25];
        String[] templis = new String[0];
        for (int i = 0;i<5 ;i++ ) {
            String masukan = input.nextLine();
            for (int j = 0;j<5 ;j++ ) {
                templis = masukan.split(" ");
                for (String elem :templis ) {
                    Number angka = new Number(Integer.parseInt(templis[j]),i,j);
                    numbers[i][j] = angka;
                    for (int x = 0;x<25 ;x++ ) {
                        nomorOri[x] = angka;
                    }
                }
            }
        }
        BingoCard kartu = new BingoCard(numbers,nomorOri);
        do {
            String masukan = input.nextLine();
            templis = masukan.split(" ");
            if (templis[0].toUpperCase().equals("MARK"))
                System.out.println(kartu.markNum(Integer.parseInt(templis[1])));
            if (templis[0].toUpperCase().equals("INFO"))
                System.out.println(kartu.info());
            if (templis[0].toUpperCase().equals("RESTART"))
                kartu.restart();
            kartu.testWinDiagonal1();
            kartu.testWinDiagonal2();
            kartu.testWinVertically();
            kartu.testWinHorizontally();
        } while (kartu.isBingo() == false);
    }
}
