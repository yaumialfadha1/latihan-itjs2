package movie;


public class Movie {
    private String judul, genre, rating, jenis;
    private int durasi;

    // Constructor
    public Movie(String judul, String rating, int durasi, String genre, String jenis) {

        this.judul = judul;
        this.rating = rating;
        this.durasi = durasi;
        this.genre = genre;
        this.jenis = jenis;

    }
    // Getter

    public String getJudul() {
        return judul;
    }

    public String getRating() {
        return rating;
    }

    public void printInfo() {
        System.out.println("------------------------------------------------------------------");
        System.out.println("Judul   : " + judul);
        System.out.println("Genre   : " + genre);
        System.out.println("Durasi  : " + durasi + " menit");
        System.out.println("Rating  : " + rating);
        System.out.println("Jenis   : Film " + jenis);
        System.out.println("------------------------------------------------------------------");
    }


}
