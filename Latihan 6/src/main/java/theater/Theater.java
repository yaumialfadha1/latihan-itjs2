package theater;


import movie.Movie;
import ticket.Ticket;
import java.util.ArrayList;

public class Theater {

    private String nama;
    private int saldo;
    public ArrayList<Ticket> ticket;
    private Movie[] film;
    private static int totalRevenue;
    private int jumlahTiket;

    // Constructor
    public Theater(String nama, int saldo, ArrayList<Ticket> ticket, Movie[] film) {
        this.nama = nama;
        this.saldo = saldo;
        totalRevenue += saldo;
        this.ticket = ticket;
        this.film = film;
        jumlahTiket = ticket.size();

    }
    // Getter
    public Movie[] getFilm() {return film;}

    public String getNama() {return nama;}

    // Setter

    public void setSaldo(int saldo) {this.saldo += saldo;}

    public void printInfo() {
        System.out.println("------------------------------------------------------------------");
        System.out.printf("Bioskop\t\t\t\t\t : %s\n", nama);
        System.out.printf("Saldo Kas\t\t\t\t : %d\n", saldo);
        System.out.printf("Jumlah tiket tersedia\t : %d\n", jumlahTiket);
        System.out.print("Daftar Film tersedia\t : ");

        for (Movie i : film) {
            System.out.print(i.getJudul() + ", ");
        }
        System.out.println("\n------------------------------------------------------------------");

    }
    
    public void printRevenue() {
        System.out.printf("Bioskop\t\t : %s\n", nama);
        System.out.printf("Saldo Kas\t : Rp. %d\n", saldo);
        System.out.println("\n------------------------------------------------------------------\n");
    }

    public static void printTotalRevenueEarned(Theater[] bioskop) {
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + totalRevenue);
        System.out.println("------------------------------------------------------------------");
        for (Theater i : bioskop) {
            i.printRevenue();
        }

    }

    public boolean filmAvailability(String judul) {
        for (Movie i : film) {
            if (i.getJudul().equals(judul)) {
                return true;
            }
        }
        return false;
    }

    public boolean cekUmur(String judul, int umur) {
        for (Movie i : film) {
            if (i.getJudul().equals(judul) && i.getRating().equals("Umum")) {
                return true;
            } else if (i.getJudul().equals(judul) && i.getRating().equals("Dewasa") && umur >= 17) {
                return true;
            } else if (i.getJudul().equals(judul) && i.getRating().equals("Remaja") && umur >= 13) {
                return true;
            }
        }
        return false;
    }

    public Ticket cekTiket(String judul, String hari, String tridi) {

        boolean tridiB = false;

        if (tridi.equals("3 Dimensi")) {
            tridiB = true;
        }

        for (Ticket i : ticket) {
            if (i.getHari().equals(hari) && tridiB == i.getTridi() && i.getFilm().getJudul().equals(judul)) {
                return i;
            }
        }
        return null;
    }

    public Movie findSpesificMovie(String judul) {
        for (Movie i : film) {
            if (i.getJudul().equals(judul)) {
                return i;
            }
        }
        return null;
    }
    public static void orderTicket(int uang) {totalRevenue += uang;}
}
