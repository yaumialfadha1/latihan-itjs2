package ticket;

import movie.Movie;

public class Ticket {

    private Movie film;
    private String hari;
    private boolean tridi;
    private int harga = 60000;
    private int jumlahTiket;

    // Constructor
    public Ticket(Movie judul, String jadwal, boolean tridi) {

        this.film = judul;
        this.hari = jadwal;
        this.tridi = tridi;
        jumlahTiket++;
        if (hari.equals("Sabtu") || hari.equals("Minggu")){harga += 40000;}

        if (tridi){harga += harga * 20 / 100;}
    }

    public int getHarga() {return harga;}

    public Movie getFilm() {return film;}

    public String getHari() {return hari;}
    
    public boolean getTridi() {return tridi;}
    
}
