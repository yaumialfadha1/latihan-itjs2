package customer;


import movie.Movie;
import theater.Theater;
import ticket.Ticket;
import java.util.Arrays;

public class Customer {

    private String nama;
    private String jenisKelamin;
    private int umur;

    // Constructor
    public Customer(String nama, String jenisKelamin, int umur) {

        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.umur = umur;
    }

    public void findMovie(Theater bioskop, String judul) {

        for (Movie i : bioskop.getFilm()) {
            if (i.getJudul() == judul) {
                i.printInfo();
                return;
            }

        }

        System.out.printf("Film %s yang dicari %s tidak ada di bioskop %s\n", judul, nama, bioskop.getNama());
    }

    public Ticket orderTicket(Theater bioskop, String judul, String hari, String tridi) {

        if (!bioskop.filmAvailability(judul)) {
            return null;
        }

        if (bioskop.cekTiket(judul, hari, tridi) == null) {
            System.out.printf("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s\n", judul, tridi, hari, bioskop.getNama());
            return null;
        }

        if (!bioskop.cekUmur(judul, umur)) {
            System.out.printf("%s masih belum cukup umur untuk menonton %s dengan rating %s\n", nama, judul, bioskop.findSpesificMovie(judul).getRating());
            return null;
        }

        Theater.orderTicket(bioskop.cekTiket(judul, hari, tridi).getHarga());
        bioskop.setSaldo(bioskop.cekTiket(judul, hari, tridi).getHarga());
        System.out.printf("%s telah membeli tiket %s jenis %s di %s pada hari %s seharga Rp. %d\n",
                nama, judul, tridi, bioskop.getNama(), hari, bioskop.cekTiket(judul, hari, tridi).getHarga());
        return bioskop.cekTiket(judul, hari, tridi);
    }

}
